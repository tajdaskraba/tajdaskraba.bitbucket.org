
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
 /*global $ */
function generirajPodatke(stPacienta) {
  var ehrId = "";
$.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: {
        "Authorization": getAuthorization()
      },
      
      success: function (data) {
        var ehrId = data.ehrId;
        var partyData;
        
        switch(stPacienta) {
          case 1: {
            var party1 =  { // bolnik1
          firstNames: "Ana",
          lastNames: "Novak",
          dateOfBirth: "1999-01-01",
          additionalInfo: {"ehrId": ehrId},
          gender: "FEMALE"
        };
            partyData = party1;
            break;
          }
          
          case 2: {
            
                    var party2 =  { // bolnik2
          firstNames: "Bojan",
          lastNames: "Oman",
          dateOfBirth: "1979-03-03",
          additionalInfo: {"ehrId": ehrId},
          gender: "MALE"
        };
            partyData = party2;
            break;
          }
          
          case 3: {
          var party3 =  { // bolnik3
          firstNames: "Gospa",
          lastNames: "Marjana",
          dateOfBirth: "1940-09-09",
          additionalInfo: {"ehrId": ehrId},
          gender: "MALE"
        };
            
            partyData = party3;
            break;
          }
        }
        
        $.ajax({ // zapise osnovne podatke k ehrId
          url: baseUrl + "/demographics/party",
          type: 'POST',
          headers: {
            "Authorization": getAuthorization()
          },
          contentType: 'application/json',
          data: JSON.stringify(partyData),
          success: function (party) { // uspesno dodani podatki
            if (party.action == 'CREATE') {
              
              // generiranje podatkov za 3 paciente
            
              switch (stPacienta) {
                case 1:
                  zapisiPodatke(ehrId, "2001-01-01T12:40:34", 100, 58, function(res){
                  zapisiPodatke(ehrId, "2005-01-01T12:40:34", 110, 58, function(res){
                  zapisiPodatke(ehrId, "2010-01-01T12:40:34", 150, 58, function(res){
                  zapisiPodatke(ehrId, "2015-01-01T12:40:34", 170, 58, function(res){ // dopolni za 4 razlicne meritve pri vseh treh pacientih
                      
                    });
                  });
                }); 
              });
                  break;
                
                case 2:
                  zapisiPodatke(ehrId, "1999-01-01T12:40:34", 168, 58, function(res){
                zapisiPodatke(ehrId, "1999-01-01T12:40:34", 168, 58, function(res){
                  zapisiPodatke(ehrId, "1999-01-01T12:40:34", 168, 58, function(res){
                    zapisiPodatke(ehrId, "1999-01-01T12:40:34", 168, 58, function(res){
                      
                    });
                  });
                }); 
              });
                  break;
                  case 3:
                    zapisiPodatke(ehrId, "1999-01-01T12:40:34", 168, 58, function(res){
                    zapisiPodatke(ehrId, "1999-01-01T12:40:34", 168, 58, function(res){
                    zapisiPodatke(ehrId, "1999-01-01T12:40:34", 168, 58, function(res){
                    zapisiPodatke(ehrId, "1999-01-01T12:40:34", 168, 58, function(res){
                      
                    });
                  });
                }); 
              });
              break;
              }
            }
          },
          error: function(err) {
          	$("#kreirajSporocilo").html("<span class='obvestilo label " +
              "label-danger fade-in'>Napaka '" +
              JSON.parse(err.responseText).userMessage + "'!");
          }
        });
      }
		});
}

function zapisiPodatke(ehrId, datum, visina, teza, callback) { // doda podatke uporabniku
  
  var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datum,
		    "vital_signs/height_length/any_event/body_height_length": visina,
		    "vital_signs/body_weight/any_event/body_weight": teza
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT'
		};
		$.ajax({
      url: baseUrl + "/composition?" + $.param(parametriZahteve),
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify(podatki),
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (res) {
       callback(res);
      },
      error: function(err) {
      	// izpisi error
      }
		});
  
}

// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija

$(document).ready(function () {
  $("#moder").hide();
  $("#gumbGen").click(function() {
    generirajPodatke(1);
    generirajPodatke(2);
    generirajPodatke(3);
    
    $("#moder").toggle();
  })
})

function myFunction() {
  document.getElementById("myDropdown").classList.toggle("show");
}

// Zaapiranje dropdowna ce kliknes nekje drugje 
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {
    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}